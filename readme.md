# Helios PHP

A server-side implementation of the Helios event tracking library. Just like the Javascript version available for client-side use, Helios PHP comes  with rollup analytics capabilities and provides an easy to use wrapper for sending data to Google Analytics from your application's backend. For the client-side implementation, please see Helios JS.

##Setup

Helios PHP is designed to be quick to implement in your application code. Simply include the library, create a new instance of the Helios class passing the necessary configuration array to the constructor and start sending analytics data to your properties with our easy to use API.

###Rollup Analytics

Just like its JS counterpart, Helios PHP handles multiple Google Analytics properties with ease. Simply pass an indexed array of additional UA identifiers to the constructor inside the configuration array, and all your events and pageviews will propagate to your desired endpoints. 

If this behaviour is unwanted or unnecessary in your use case, simply leave the field blank and Helios will only send data to your primary account.

###SSL Support

Google's Measurement Protocol allows both HTTP and HTTPS connections to be made to the Analytics servers. However, both protocols require different endpoints to be used when sending data to Google. Helios knows which one to use and when, all you need to do is pass a parameter called `useSSL` with your configuration array and set it to true or false depending on your application's needs.

If no parameter is passed to the constructor, the value of `useSSL` defaults to `false`.

###Local logging

The constructor method takes an optional second parameter which defines the path to the local log file where all analytics activity will be logged. This is useful to have access to your data in case something goes wrong between your application and the Analytics servers.

If no logfile path is passed to the constructor, Helios will not log any activity locally.

###Configuration
Here are the possible configuration keys and values that Helios can accept on initialization:
```php
array(
	'primary' => 'UA-XXXX-YY'       // Required. GA tracking code.
	'secondary' => array(
		'UA-YYYY-ZZ',
		...
		),                          // Optional. An array of GA codes.
	'clientID' => 'xxxx-yyyy-zzzz', // Optional. A persistent UUID.
	'useSSL' => true                // Optional. Defaults to false.
)
```
##API
####sendPageView(hostname, page, title)
The `sendPageView()` method sends a single pageview to your analytics trackers originating from the hostname passed as first parameter, with a destination page and title defined by the second and third parameters of the call. 
All parameters are required for the method to execute successfully.
####sendEvent(category, action, label, value)
The `sendEvent()` method sends event data to Google Analytics in real time. Just like typical GA event tracking implementations, this method requires an event to have a category and action associated with them, while the label and value parameters are optional. 
If you don't need the label or value parameters to be sent to Google, you can pass `NULL` or leave the parameters empty.
##Full example
The below code sample is an example implementation of Helios PHP using dummy config. Replace the configuration values with your own tracking IDs to use this sample in your own code.
```php
require 'helios-backend.php';

$config = array(
	'primary' => 'UA-1234-56',
	'secondary' => array(
		'UA-7891-01',
		'UA-1121-13'
	),
	'clientID' => 'abcd-efgh-ijkl',
	'useSSL'   => true
);
$logfile = 'path/to/logfile.log';

$helios = new Helios($config, $logfile);

//Sends a pageview to all linked properties with the hostname, page and title specified.
$helios->sendPageView('http://domain.com', '/page', 'Example Page');

//Sends an event signaling the action 'eat' in the category 'bananas' labeled 'i-ate-a-banana'. The event has a value of 100 associated with it.
$helios->sendEvent('bananas', 'eat', 'i-ate-a-banana', 100);
```
###Done!
This is all you need to be able to start sending analytics data from your server-side PHP code to your web properties. Now get out there and eat those bananas!