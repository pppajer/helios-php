<?php

/*

Helios PHP

*/

class Helios {

	private $primary;
	private $secondary;
	private $cid;
	private $logfile;
	private $GAEndpoint;

	public function __construct($config, $logfile = NULL) {

		$primary 	= $config['primary'];
		$secondary 	= isset($config['secondary']) ? $config['secondary'] : NULL;
		$cid 		= isset($config['clientID']) ? $config['clientID'] : NULL;
		$ssl 		= (isset($config['useSSL']) && ($config['useSSL'] === true)) ? true : false;

		if ($cid === NULL) {
			
			$pseudoRandom = new DateTime();
			$pseudoRandom = $pseudoRandom->format('U');

			$cid = $pseudoRandom;
		}

		if ($ssl === true) {
			
			$this->GAEndpoint = 'https://ssl.google-analytics.com/collect';

		} else {

			$this->GAEndpoint = 'http://www.google-analytics.com/collect' ;

		}
		
		$this->primary 		= $primary;
		$this->secondary 	= $secondary;
		$this->cid 			= $cid;
		$this->logfile 		= $logfile;

	}

	public function sendPageview($host, $page, $title) {

		$pageData = array(
			't' 	=> 'pageview',
			'dh' 	=> $host,
			'dp' 	=> $page,
			'dt' 	=> $title
			);

		$this->propagateData($pageData);

	}

	public function sendEvent($category, $action, $label = NULL, $value = NULL) {
		
		$eventData = array(
			't' 	=> 'event',
			'ec' 	=> $category,
			'ea' 	=> $action
			);

		if ($label !== NULL) {
			$eventData['el'] = $label;
		}

		if ($value !== NULL) {
			$eventData['ev'] = $value;
		}

		$this->propagateData($eventData);


	}

	private function buildPayload($data, $tracker = NULL) {

		$payload = array(
			'v' => '1',
			'cid' => $this->cid
			);
		$payload['tid'] = ($tracker === NULL) ? $this->primary : $tracker;

		$payload = array_merge($payload, $data);
		$payload = http_build_query($payload);

		return $payload;
	}

	private function propagateData($data) {

		$payload 	= $this->buildPayload($data);
		$hitStatus 	= $this->sendHit($payload);
		$this->logActivity($hitStatus, $payload);

		if ($this->secondary !== NULL) {
			
			foreach ($this->secondary as $tracker) {
				
				$payload 	= $this->buildPayload($data, $tracker);
				$hitStatus 	= $this->sendHit($payload);

				$this->logActivity($hitStatus, $payload);
			}

		}
	}

	private function sendHit($payload) {
		
		$request = curl_init();

		$url = sprintf('%s?%s', $this->GAEndpoint, $payload);

		var_dump($url);

		curl_setopt($request, CURLOPT_URL, $url);
		curl_setopt($request, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
		curl_setopt($request, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($request, CURLOPT_HEADER, true);
		curl_setopt($request, CURLOPT_NOBODY, true);

		$response 	= curl_exec($request);
		$resCode 	= curl_getinfo($request, CURLINFO_HTTP_CODE);

		curl_close($request);

		if ($resCode == '200') {
			
			return true;

		} else {

			return false;

		}
	}

	private function logActivity($response, $request) {

		if ($this->logfile === NULL) {
			
			return;

		} else {

			$timestamp 		= new DateTime();
			$timestamp 		= $timestamp->format('Y-m-d H:i:s');
			$formatString 	= "%s - %s transfer with payload: '%s'.";
			$status 		= ($response === false) ? 'FAILED' : 'SUCCESSFUL';

			$loggedString 	= sprintf($formatString, $timestamp, $status, $request);
			$loggedString 	= $loggedString.PHP_EOL;

			file_put_contents($this->logfile, $loggedString, FILE_APPEND);
		
		}
		
	}


}

?>